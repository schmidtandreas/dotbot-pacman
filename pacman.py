import sys
import os
import subprocess
import dotbot
from enum import Enum

class PkgStatus(Enum):
    UP_TO_DATE = 'Up to date'
    INSTALLED = 'Installed'
    ERROR = 'Errors occurred'
    NOT_SURE = 'Could not determine'

class Pacman(dotbot.Plugin):
    _directives = ['pacman-native']

    def __init__(self, context):
        super(Pacman, self).__init__(self)
        self._context = context
        self._strings = {}
        self._strings[PkgStatus.UP_TO_DATE] = 'is up to date -- skipping'
        self._strings[PkgStatus.INSTALLED] = 'Total Installed Size'
        self._strings[PkgStatus.ERROR] = 'error:'

    def can_handle(self, directive):
        return directive in self._directives

    def handle(self, directive, data):
        if not self.can_handle(directive):
            raise ValueError('Pacman cannot handle directive %s' % directive)
        return self._process_packages(directive, data)

    def _process_packages(self, directive, packages):
        results = {}
        successful = [PkgStatus.UP_TO_DATE, PkgStatus.INSTALLED]

        for pkg in packages:
            result = self._install(pkg)
            results[result] = results.get(result, 0) + 1
            if result not in successful:
                self._log.error('Could not install package {}'.format(pkg))

        if all([result in successful for result in results.keys()]):
            self._log.info('All pacman packages installed successfully')
            success = True
        else:
            success = False

        for status, amount in results.items():
            log = self._log.info if status in successful else self._log.error
            log('{} {}'.format(amount, status.value))

        return success

    def _install(self, pkg):
        cmd = 'LANG=en_US.UTF-8 pacman --needed --noconfirm -S {}'.format(pkg)

        self._log.info('Installing {}'.format(pkg))

        process = subprocess.Popen(cmd, shell=True,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT)
        out = []

        while True:
            line = process.stdout.readline().decode('utf-8')
            if not line:
                break
            out.append(line)
            self._log.lowinfo(line.strip())
            sys.stdout.flush()

        process.stdout.close()

        process.wait()

        out = ''.join(out)

        for status in self._strings.keys():
            if out.find(self._strings[status]) >= 0:
                if status == PkgStatus.INSTALLED:
                    if process.returncode == 0:
                        return status
                    else:
                        return PkgStatus.ERROR
                return status

        self._log.warning('Could not determine what happened with package {}'.format(pkg))
        return PkgStatus.NOT_SURE

