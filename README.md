# Dotbot `pacman` Plugin

Plugin for [dotbot](https://github.com/anishathalye/dotbot) that adds a `pacman`
directive. It allows installation of packeges using `pacman`.

## Installation

Add it as a submodule of your dotfiles repository.

```bash
git submodule add https://gitlab.com/schmidtandreas/dotbot-pacman
```

## Usage

One option is having your packages list in a separate file. This way you can run it separately
from your main configuration.

```bash
./install -p dotbot-pacman/pacman.py -c packages.conf.yaml
```

Alternatively, you can add the directive directly in `install.conf.yaml` and
modify your `install` script so it automatically enables the `pacman` plugin.

```bash
"${BASEDIR}/${DOTBOT_DIR}/${DOTBOT_BIN}" -d "${BASEDIR}" -c "${CONFIG}" \
  --plugin-dir "${BASEDIR}/dotbot-pacman" "${@}"
```

### Sudo

This plugin do not use sudo implecite.
For installation of packages it's recomended to use the [sudo](https://github.com/DrDynamic/dotbot-sudo) plugin.

### AUR

The packages from AUR repository are not supported by the `pacman` plugin.
For installation of AUR packages it's recomended to use the [yay](https://github.com/oxson/dotbot-yay) plugin.

## Configuration

Example of the `packages.conf.yaml`:

```yaml
- sudo
  - pacman:
    - neovim
    - alacritty
    - git
```

